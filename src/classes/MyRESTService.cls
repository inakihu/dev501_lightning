@RestResource (urlMapping='/v1/*')
global class MyRESTService {
	/*@HttpPost
	global static Id doPost (String cLastName, Account a) { 
		Contact c= new Contact(lastName = cLastName, AccountId = a.Id);
		insert c;
		return c.id;
	}*/

	@HttpGet
	global static returnClassContact getContact () {
		System.debug(RestContext.request.params);
		Id IdContact = RestContext.request.params.get('Id');
		Contact c = [SELECT Id, FirstName, lastName, AccountId FROM Contact WHERE Id = :IdContact LIMIT 1];
		returnClassContact rClassContact = new returnClassContact();
		rClassContact.success = 'true';
		rClassContact.message = '';
		rClassContact.record = c;
		return rClassContact;
	}
	
	global class returnClassContact {
	    global String success;
	    global String message;
	    global Contact record;
		
		global returnClassContact(){
			this.success = 'true';
			this.message = '';
			this.record = new Contact();
		}
		
	}

	@HttpPost
	global static returnClassCases getOpenCases() {
		System.debug('Params:' + RestContext.request.params);
		String companyName = RestContext.request.params.get('companyName'); 
		Account company = [ Select ID, Name, BillingState 
				from Account
				where Name= :companyName]; 
		List<Case> cases = [SELECT Id, Subject, Status, OwnerId, Owner.Name 
				FROM Case 
				WHERE AccountId=: company.Id];
		
		returnClassCases rClassCases = new returnClassCases();
		
		rClassCases.success = 'true';
		rClassCases.message = '';
		rClassCases.record = company;
		rClassCases.records = cases;
   		return rClassCases;
   	}
   	
   	global class returnClassCases {
	    global String success;
	    global String message;
	    global Account record;
	    global List<Case> records;
		
		global returnClassCases(){
			this.success = 'true';
			this.message = '';
			this.records = new List<Case>();
		}
	}
}