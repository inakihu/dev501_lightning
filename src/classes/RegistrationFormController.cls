public with sharing class RegistrationFormController {
	// controller="RegistrationFormController"
	// controller="DEV501LE_IHU.RegistrationFormController"
	@AuraEnabled
    public static Contact registerAsContact(Contact c) {
    	insert c;
    	return c;
    }
    
    @AuraEnabled
    public static User getServerUserData() {
    	return [SELECT Id, FirstName, LastName, Phone FROM User WHERE Id= :UserInfo.getUserId() LIMIT 1]; 
    }
    
}