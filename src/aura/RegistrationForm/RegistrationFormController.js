({
    handleClick : function(cmp, event) {
        var attributeValue = cmp.get("v.text");
        console.log("current text: " + attributeValue);
		//debugger;
        var target;
        if (event.getSource) {
            // handling a framework component event
            target = event.getSource(); // this is a Component object
            cmp.set("v.text", target.get("v.label"));
        } else {
            // handling a native browser event
            target = event.target.value; // this is a DOM element
            cmp.set("v.text", event.target.value);
        }
        alert("handleClick");
	},
    
    
    
    handleSubmitClick : function(component, event, helper) {
		console.log("handleSubmitClick");
        var action = component.get("c.registerAsContact");
        action.setParams({
	      "c": component.get("v.newContact")
		});

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("state: " + state);
            if (state === "SUCCESS") {
                var c = response.getReturnValue();
                alert("Inserted: " + c.Id);
            }else if (state === "ERROR") {
                debugger;
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message) {
                        console.log("Error message: " + errors[0].pageErrors[0].message);
                        alert("Error message: " + errors[0].pageErrors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
    
    
    
    getUserData : function(component, event, helper) {
        console.log("getUserData");
        var action = component.get("c.getServerUserData");
        action.setCallback(this, function(response) {
            var state = response.getState();
            //debugger;
            if (state === "SUCCESS") {
                var u = response.getReturnValue();
                component.set("v.newContact.FirstName", u.FirstName);
                component.set("v.newContact.LastName", u.LastName);
                component.set("v.newContact.Salutation", "");
                component.set("v.newContact.Phone", u.Phone);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        alert("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert("Unknown error");
                }
            }
        });
		
		$A.enqueueAction(action);
        
	}
	
})